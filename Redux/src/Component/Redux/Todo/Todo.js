import React, {useState} from 'react';
// import './Todo6.css'
import {addTodo, removeTodo, editTodo} from "../../Action/todoSlice";
import {useDispatch, useSelector} from "react-redux";

function Todo(props) {
    const todos = useSelector((reducer) => reducer.todo.todos)
    const dispatch = useDispatch()
    const [toInput, setToInput] = useState('')
    const [newtoInput, setNewToInput] = useState('')
    const [showEdit, setShowEdit] = useState(-1);


    function buttonVale(e) {
        dispatch(
            addTodo({
                id: Math.floor(Math.random() * 1000),
                text: toInput
            })
        )
        setToInput('')
    }

    function deletee(id) {
        dispatch(
            removeTodo(id)
        )
    }

    function editItem(id, newText) {
        dispatch(
            editTodo({
                id: Math.floor(Math.random() * 1000),
                text: newtoInput
            })
        )
        setNewToInput('')
    }



    return (
        <div className={"mainQQ"}>
            <div className={"mainTD"}>
                <div className={"divOnIB"}>
                    <input value={toInput} onChange={(e) => setToInput(e.target.value)} type={"text"}
                           className={"input1"}/>
                    <button onClick={buttonVale} type={"submit"} className={"button1"}>Add</button>
                </div>
                <div className={"divCN"}>
                    <ul>
                        {todos.map((item) => {
                            return (
                                <div>
                                    <li className={'li'} key={item.id}>
                                        {item.text}
                                        <div>
                                            <button className="buttonDel" onClick={() => deletee(item.id)}>Delete
                                            </button>
                                            <button className={"buttonDel"} onClick={() => setShowEdit(item.id)}>Edit
                                            </button>
                                        </div>
                                    </li>
                                    {showEdit === item.id ? (
                                        <div>
                                            <input className={"inputUpdate"} type="text" value={newtoInput}
                                                   onChange={(e) => setNewToInput(e.target.value)}/>
                                            <button className={"buttonDel"} onClick={() => editItem()}>Update</button>
                                        </div>
                                    ) : null}

                                </div>

                            );

                        })}
                    </ul>
                </div>
            </div>
        </div>
    );
}

export default Todo;
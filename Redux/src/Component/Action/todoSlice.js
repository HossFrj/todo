import { createSlice } from '@reduxjs/toolkit';



const todoSlice = createSlice({
    name: 'todo',
    initialState: {
        todos : [],
    },
    reducers: {
        addTodo: (state, action) => {
            state.todos.push(action.payload)
        },
        removeTodo: (state, action) => {
        state.todos = state.todos.filter((item)=>item.id !== action.payload)
        },
        editTodo: (state, action) => {
            state.todos = state.todos.filter((item)=>item.id ===action.payload)
            state.todos.push(action.payload)
        }
    },
});

export const { addTodo, removeTodo,editTodo } = todoSlice.actions;
export default todoSlice.reducer;

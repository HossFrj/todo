import {createSlice} from "@reduxjs/toolkit";


const cardSlice = createSlice({
    name : 'card',
    initialState:{
        cards :[],
        count : 0
    },
    reducers:{
        addToCard : (state,action)=>{
            state.cards.push(action.payload)
        },
        incCount : (state,action)=>{
            state.count +=1
        },dicCount : (state,action)=>{
            state.count -=1
        },resCount : (state,action)=>{
            state.count =0
        },deleteCard : (state,action)=>{
            state.cards = state.cards.filter((list)=>list.id !== action.payload)
        },editCard : (state,action)=>{
            state.cards = state.cards.filter((list)=>list.id === action.payload)
            state.cards.push(action.payload)
        },
        // showEdit : (state,action)=>{
        //     state.cards = state.cards.filter((list)=>list.id === action.payload)
        // }
    }


})
export const {editCard,deleteCard,addToCard, incCount,dicCount,resCount}=cardSlice.actions
export default cardSlice.reducer
import React, {useState} from 'react';
import './Todo6.css'

function Todo6(props) {
    const [add, setAdd] = useState("")
    const [tasks, setTasks] = useState([])
    const [showEdit, setShowEdit] = useState(-1);
    const [updatedText, setUpdatedText] = useState("");


    function getValue(e) {
        setAdd(e.target.value)


    }

    function buttonVale(e) {
        tasks.map((iii) => {
            if (iii.value === add) {
                const newArray = tasks.filter((del) => del.value !== add)
                setTasks(newArray)
                alert("repetitious!!! please enter new value again")
                // console.log(tasks)
            } else {

            }
        })
        if (add.length > 10) {
            alert("should be input value length < 10")
        } else {
            if (add === "") {
            } else {

                // let tmp = [...tasks]
                // tmp.push(add)
                // setTasks(tmp)
                //  setTasks(prev => [...tasks, add])
                const item = {
                    id: Math.floor(Math.random() * 1000),
                    value: add,
                };

                // for(let x=0 ; x<tasks.length;x++){
                //     if(tasks[x]===tasks[x-1]){
                //
                //     }
                //
                // }
                setTasks((oldList) => [...oldList, item]);
                setAdd("")


            }
        }
    }

    function deletee(id) {
        // console.log(id)
        // console.log(tasks)
        const newArray = tasks.filter((del) => del.id !== id)
        setTasks(newArray)
    }

    function editItem(id, newText) {
        tasks.map((iii) => {
            if (iii.value === updatedText) {
                alert("repetitious!!! please enter new value again")
                return newText = ""

            }
        })

        if (newText.length > 10) {
            alert("should be input value length < 10")
        } else {

            if (newText === "") {

            } else {
                const currentItem = tasks.filter((item) => item.id === id);
                const newItem = {
                    id: currentItem.id,
                    value: newText,
                };
                deletee(id);
                setTasks((oldList) => [...oldList, newItem]);
                setUpdatedText("");
                setShowEdit(-1);
            }
        }
    }


    return (
        <div className={"mainQQ"}>
            <div className={"mainTD"}>
                <div className={"divOnIB"}>
                    <input value={add} onChange={getValue} type={"text"} className={"input1"}/>
                    <button onClick={buttonVale} type={"submit"} className={"button1"}>Add</button>
                </div>
                <div className={"divCN"}>
                    <ul>
                        {tasks.map((item) => {
                            return (
                                <div>
                                    <li className={'li'} key={item.id}>
                                        {item.value}
                                        <div>
                                            <button className="buttonDel" onClick={() => deletee(item.id)}>Delete
                                            </button>
                                            <button className={"buttonDel"} onClick={() => setShowEdit(item.id)}>Edit
                                            </button>
                                        </div>
                                    </li>
                                    {showEdit === item.id ? (
                                        <div>
                                            <input className={"inputUpdate"} type="text" value={updatedText}
                                                   onChange={(e) => setUpdatedText(e.target.value)}/>
                                            <button className={"buttonDel"}
                                                    onClick={() => editItem(item.id, updatedText)}>Update
                                            </button>
                                        </div>
                                    ) : null}

                                </div>

                            );

                        })}
                    </ul>
                </div>
            </div>
        </div>
    );
}

export default Todo6;
import React, {useState} from 'react';
import './Todo6.css'

interface TodoProps{
}
interface task0 {
    id : number,
    value : string
}
interface task1 {
    id : number ,
    value : string
}
const Todo : React.FC = (props:TodoProps) =>  {
    const [add, setAdd] = useState<string>("")
    const [tasks, setTasks] = useState<task0[]>([])
    const [showEdit, setShowEdit] = useState<number>(-1);
    const [updatedText, setUpdatedText] = useState<string>("");


    function getValue(e :React.ChangeEvent<HTMLInputElement>) {
        setAdd(e.target.value)


    }

    function buttonVale(e : React.MouseEvent<HTMLButtonElement>) {
        tasks.map((iii)=>{
            if(iii.value===add){
                const newArray = tasks.filter((del)=>del.value !== add)
                setTasks(newArray)
                alert("repetitious!!! please enter new value again")
                // console.log(tasks)
            }else {

            }
        })
        if(add.length>10){
            alert("should be input value length < 10")
        }else{
            if(add === ""){
            }else {

                // let tmp = [...tasks]
                // tmp.push(add)
                // setTasks(tmp)
                //  setTasks(prev => [...tasks, add])
                const item = {
                    id: Math.floor(Math.random() * 1000),
                    value: add,
                };

                // for(let x=0 ; x<tasks.length;x++){
                //     if(tasks[x]===tasks[x-1]){
                //
                //     }
                //
                // }
                setTasks((oldList) => [...oldList, item]);
                setAdd("")



            }}}

    function deletee(id : number) {
        // console.log(id)
        // console.log(tasks)
        const newArray = tasks.filter((del)=>del.id !== id)
        setTasks(newArray)
    }
    function editItem(id :number, newText : string) {
        tasks.map((iii)=> {
            if(iii.value===updatedText){
                alert("repetitious!!! please enter new value again")
                return   newText=""

            }
        })

        if(newText.length>10){
            alert("should be input value length < 10")
        }else {

            if(newText===""){

            }
            else{
                const currentItem  = tasks.filter((item) => item.id === id);
                const newItem : task1  = {
                    id: currentItem[0].id,
                    value: newText,
                };
                deletee(id);
                setTasks((oldList) => [...oldList, newItem]);
                setUpdatedText("");
                setShowEdit(-1);
            }}}


    return (
        <div className={"mainQQ"}>
            <div className={"mainTD"}>
                <div className={"divOnIB"}>
                    <input value={add} onChange={getValue} type={"text"} className={"input1"}/>
                    <button onClick={buttonVale} type={"submit"} className={"button1"}>Add</button>
                </div>
                <div className={"divCN"}>
                    <ul>
                        {tasks.map((item) => {
                            return (
                                <div>
                                    <li className={'li'} key={item.id} >
                                        {item.value}
                                        <div>
                                            <button className="buttonDel" onClick={() => deletee(item.id)}>Delete</button>
                                            <button className={"buttonDel"} onClick={() => setShowEdit(item.id)}>Edit</button>
                                        </div>
                                    </li>
                                    {showEdit === item.id ? (
                                        <div>
                                            <input className={"inputUpdate"} type="text" value={updatedText} onChange={(e) => setUpdatedText(e.target.value)}/>
                                            <button className={"buttonDel"} onClick={() => editItem(item.id, updatedText)}>Update</button>
                                        </div>
                                    ) : null}

                                </div>

                            );

                        })}
                    </ul>
                </div>
            </div>
        </div>
    );
}

export default Todo;